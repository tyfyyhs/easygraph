declare class easyGraph {
    ClassName: string;
    maxWidth: number;
    maxHeight: number;
    graphWidth: number;
    graphHeight: number;
    config: graphConfig;
    id: HTMLElement;
    tippy: any;
    tippyInstance: any;
    /**
     *
     * @param ID 그래프를 표시할 ID입니다.
     * @param config 그래프 설정값입니다.
     * @param tippy 툴팁 라이브러리입니다.
     */
    constructor(ID: any, tippy: any);
    load(config: graphConfig): this;
    tipLoad(): void;
    draw(): Promise<void>;
    ajaxLoad(config: any): Promise<unknown>;
    createSVG(): Promise<void>;
    svgWave(): HTMLElement[];
    /**
     * 노드를 만듭니다
     * @param Tag 요소의 태그명
     * @param Class 요소에 부여할 클래스명
     * @param Style 요소에 부여할 스타일
     * @returns 노드를 반환합니다.
     */
    createNode(Tag: string, Class: string[], Style: string): HTMLElement;
    /**
     * 특정 노드의 자식을 입력합니다.
     * @param target 노드를 입력할 대상 노드
     * @param nodeArray 입력될 노드의 배열
     */
    appendNode(target: HTMLElement, nodeArray: Array<Node>): void;
    /**
     * 타입이 bar인 경우에 bar그래프를 만듭니다.
     */
    createBar(): void;
    /**
     * 타입이 bar인 경우에 입력받은 각 수치값을 계산하여 백분율로 반환합니다.
     * @returns 입력값과 동일한 오브젝트에서 값을 백분율로 수정 후 반환합니다.
     */
    barShadowWidthCalc(config: any): any;
    /**
     * 그래프의 그림자를 표현하는 Node를 생성합니다.
     * @param config 전역 설정값
     * @returns 사이즈가 조정된 그림자 Node
     */
    barShadow(config: graphConfig): any[];
    /**
     * 그래프 값을 표시합니다.
     * @param config 설정값
     */
    barLine(config: any): HTMLElement[];
}
declare class graphConfig {
    type: string;
    bar: barObject;
    svg: svgInterCeptObject;
    value: number;
    height: string;
    barHeight: string;
    width: string;
    color: string;
    radius: number;
    opacity: number;
    animate: Boolean;
    tooltip: any;
}
declare class barObject {
    safe: number;
    warning: number;
    danger: number;
}
declare class svgInterCeptObject {
    url: string;
    Data: Object;
}
declare class InterCeptData {
    safe: Array<any>;
    warning: Array<any>;
    danger: Array<any>;
}
