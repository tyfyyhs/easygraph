
class easyGraph {
    ClassName='';
    maxWidth = 0;
    maxHeight = 0;
    graphWidth = 0;
    graphHeight = 0;
    config: graphConfig;
    id: HTMLElement;
    tippy;
    tippyInstance: any=''
    /**
     * 
     * @param ID 그래프를 표시할 ID입니다.
     * @param config 그래프 설정값입니다.
     * @param tippy 툴팁 라이브러리입니다.
     */
    constructor(ID, tippy) {
        this.id = document.querySelector(ID);
        this.tippy =tippy;
    }
    
    load(config:graphConfig){
        
        this.id.innerHTML="";
        this.config = config;
        this.draw();
        return this;
    }
    
    tipLoad(){
        if(this.tippyInstance!==''){
            this.tippyInstance.destroy();
        }
        this.config.tooltip.theme=this.ClassName;
        console.log(this.config.tooltip)
        this.tippyInstance = this.tippy(this.id, this.config.tooltip);
    }

    async draw(){

        if (this.config.type === 'bar') {
            this.id.style.cssText = 'width: 100%; position: relative; line-height: 30px' +
            ';width: ' + this.config.width +
            ';background-color: ' + this.config.color +
            ';line-height: ' + this.config.height;
            this.createBar();
        } else if(this.config.type === 'svg') {
            this.id.style.cssText = 'width: 260px; position: relative; height: 260px;overflow: hidden;display: inline-block;padding: 0;' +
            ';width: ' + this.config.width +
            ';background-color: ' + this.config.color +
            ';height: ' + this.config.height;
            this.createSVG();
        }
    }



    ajaxLoad(config){
        
        return new Promise(function (resolve, reject) {
            var ajax = new XMLHttpRequest()
            ajax.open('GET', config.svg.url, true)
            ajax.onload = function(e) {
                if (this.status >= 200 && this.status < 300) {
                    resolve(ajax.response);
                } else {
                    reject({
                        status: this.status,
                        statusText: ajax.statusText});}}
            ajax.onerror = function () {
                reject({
                    status: this.status,
                    statusText: ajax.statusText});};
            ajax.send();
        });


    }

    async createSVG(){ 
        const easyGraph_shadow: HTMLElement =
            this.createNode('div', ['easyGraph_shadow'], '');

        await this.ajaxLoad(this.config).then((e:string)=>{
            easyGraph_shadow.innerHTML = e;
            easyGraph_shadow.querySelector('svg').style.cssText='z-index: 100;position: absolute;height:'+
            this.config.height+';width:'+
            this.config.width+';'
        })
        const node: SVGElement = easyGraph_shadow.querySelector('svg')        
        console.log(this.graphHeight)

        this.appendNode(this.id, [node]);
        this.appendNode(this.id, this.svgWave());
        setTimeout(() => {
            console.log(document.querySelector('.wave'));
            ( < HTMLElement > document.querySelector('.wave')).style.height = (this.graphHeight + 100) + '%';
            this.tipLoad();
        }, 500);
    }



    svgWave(){
        let level1 = 0 // 값 범위
        let level2 = 0 // 퍼센트 범위
        let loopCnt = 1 // 루프횟수
        const size = Object.keys(this.config.svg.Data).length;

        for (const key in this.config.svg.Data) {
            if (this.config.svg.Data.hasOwnProperty(key)) {
                const InterCept = parseInt(this.config.svg.Data[key][1].split('%')[0]);
                if (level1 <= this.config.value && this.config.value < level1 + this.config.svg.Data[key][0]) {
                    this.ClassName = key;
                    // 임계퍼센트/100 * (값/임계치(절편값)*100) = 임계퍼센트 안에서의 높이
                    this.graphHeight = InterCept/100*((this.config.value-level1)/this.config.svg.Data[key][0]*100)
                    console.log(InterCept)
                    console.log(this.graphHeight)
                    console.log(loopCnt)
                    this.graphHeight += level2; // 이전 절편 값을 더함 
                    console.log(this.config.svg.Data[key][0])
                    console.log(level2)
                    console.log(this.graphHeight)

                    break;
                } else if (size===loopCnt && level1 <= this.config.value) {
                    this.ClassName = Object.keys(this.config.svg.Data)[size - 1];
                    this.graphHeight = 100;
                } else {
                    level1 += this.config.svg.Data[key][0];
                    level2 += InterCept;
                    loopCnt++;
                }
            }
        }
        const wave = this.createNode('span',['wave',this.ClassName],
        'left: 10%;z-index: 0;position: absolute; bottom:-100%; width: 100%; height: 100%;border-radius:' + this.config.radius + '%;'
        )
        console.log(wave);
        return [wave]

    }



    /**
     * 노드를 만듭니다
     * @param Tag 요소의 태그명
     * @param Class 요소에 부여할 클래스명
     * @param Style 요소에 부여할 스타일
     * @returns 노드를 반환합니다. 
     */
    createNode(Tag: string, Class: string[], Style: string) {
        const node = document.createElement(Tag);
        Class.forEach(element => {
            node.classList.add(element);
        });
        this.config.animate ? node.style.cssText = Style + ';transition: all 700ms cubic-bezier(.32,2,.55,.27);' :
            node.style.cssText = Style;
        return node;
    }

    /**
     * 특정 노드의 자식을 입력합니다.
     * @param target 노드를 입력할 대상 노드
     * @param nodeArray 입력될 노드의 배열
     */
    appendNode(target: HTMLElement, nodeArray: Array < Node > ) {
        nodeArray.forEach(node => {
            target.appendChild(node);
        });
    }

    /**
     * 타입이 bar인 경우에 bar그래프를 만듭니다.
     */
    createBar() {
        const easyGraph_shadow: HTMLElement =
            this.createNode('div', ['easyGraph_shadow'], 'width: calc(100% - 6px); padding-left: 3px; display: inline-block;');
        this.appendNode(easyGraph_shadow, this.barShadow(this.config));
        this.appendNode(easyGraph_shadow, this.barLine(this.config));
        this.id.appendChild(easyGraph_shadow);
        setTimeout(() => {
            ( < HTMLElement > document.querySelector('.LineNode')).style.width = (this.graphWidth * 100) + '%';
            ( < HTMLElement > document.querySelector('.TipNode')).style.left = 'calc(' + (this.graphWidth * 100) + '% - 5px)';
            this.tipLoad();
        }, 500);
    }

    /**
     * 타입이 bar인 경우에 입력받은 각 수치값을 계산하여 백분율로 반환합니다.
     * @returns 입력값과 동일한 오브젝트에서 값을 백분율로 수정 후 반환합니다.
     */
    barShadowWidthCalc(config) {
        let maxWidth = 0;
        for (const key in config.bar) {
            if (config.bar.hasOwnProperty(key)) {
                const element = config.bar[key];
                maxWidth += element;
            }
        }
        for (const key in config.bar) {
            if (config.bar.hasOwnProperty(key)) {
                config.bar[key] = config.bar[key] / maxWidth;
            }
        }
        this.maxWidth = maxWidth;
        return config;
    }

    /**
     * 그래프의 그림자를 표현하는 Node를 생성합니다.
     * @param config 전역 설정값
     * @returns 사이즈가 조정된 그림자 Node
     */
    barShadow(config:graphConfig) {
        const C = this.barShadowWidthCalc(config);
        const Nodes = [];
        const size = Object.keys(config.bar).length;
        let l = 1;
        for (const key in config.bar) {
            if (config.bar.hasOwnProperty(key)) {
                const element = config.bar[key];
                if (l === 1) {
                    Nodes.push(this.createNode('div', [key], 'opacity:' + config.opacity + ';display: block;float: left;border-radius: ' + config.radius + 'px 0px 0px ' + config.radius + 'px;' +
                        'height:' + config.barHeight +
                        ';width:' + (element * 100) + '%;'
                    ));
                } else if (l === size) {
                    Nodes.push(this.createNode('div', [key], 'opacity:' + config.opacity + ';display: block;border-radius: 0px ' + config.radius + 'px ' + config.radius + 'px 0px;float: left;' +
                        'height:' + config.barHeight +
                        ';width:' + (element * 100) + '%;'
                    ));
                } else {
                    Nodes.push(this.createNode('div', [key], 'float: left;opacity:' + config.opacity + ';display: block;' +
                        'height:' + config.barHeight +
                        ';width:' + (element * 100) + '%;'
                    ));
                }
                l++;
            }
        }
        return Nodes;
    }
    /**
     * 그래프 값을 표시합니다.
     * @param config 설정값
     */
    barLine(config) {
        this.graphWidth = config.value / this.maxWidth;
        let level1 = 0
        const size = Object.keys(config.bar).length;
        for (const key in config.bar) {
            if (config.bar.hasOwnProperty(key)) {
                if (level1 <= this.graphWidth && this.graphWidth <= level1 + config.bar[key]) {
                    this.ClassName = key;
                    break;
                } else if (this.graphWidth === 1) {
                    this.ClassName = Object.keys(config.bar)[size - 1];
                    break;
                } else {
                    level1 += config.bar[key];
                }
            }
        }

        var lineNode = this.createNode('div', [this.ClassName, 'LineNode'], 'display: block; position: relative;border-radius: ' + config.radius + 'px;' +
            'height:' + config.barHeight +
            ';width:0%;'
        );

        var tipNode =  this.createNode('div', [this.ClassName,'TipNode'],'background-color:#FFFFFF00;top: 0px; position: absolute;width: 0;height: 0;'+
        'border-width: 8px 4px 0px 4px;border-style: solid;'
        );

        return [lineNode, tipNode];
    }


}

class graphConfig {
    type: string; // 2020/05/02 기준 사용 가능한 값: bar, svg 
    bar: barObject;
    svg: svgInterCeptObject;
    value: number; // 실제 표시될 데이터를 넣습니다.
    height: string; // 전체 높이를 지정합니다. 
    barHeight: string; // type이 bar인 경우 bar의 높이입니다. 전체 높이의 1/3이 적당합니다.
    width: string; // 전체 넓이를 지정합니다. 
    color: string; // 백그라운드 색상을 지정합니다. 
    radius: number; // 그래프의 둥글기를 설정합니다.
    opacity: number; // 그림자의 투명도를 설정합니다.
    animate: Boolean; // 애니메이션 여부 // 애니메이션 여부
    tooltip: any // 툴팁 라이브러리 설정 오브젝트
}
class barObject {
    safe: number;
    warning: number;
    danger: number;
}
class svgInterCeptObject {
    url: string; // 이미지의 url을 넣습니다. 이미지는 샘플 이미지와 같은 svg만 사용 가능합니다.
    Data: Object; //class InterCeptData, 데이터의 절편 수치
}
class InterCeptData {
    safe: Array<any>;
    warning: Array<any>;
    danger: Array<any>;
}
